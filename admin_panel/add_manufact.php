<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
  
	include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='manufact.php' >Производители</a> \ <a> Добавление производителя</a>";

  if ($_POST["submit_add"]) {
    $error = array();

    if (!$_POST["form_title"]) {
      $error[] = "Укажите название производителя";
    }

    if (count($error)) {
      $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
    }
    else {
        $newBrand = translit($_POST["form_title"]);
        $mfrpath = '../products/'.$newBrand.'.php';
        mysql_query("INSERT INTO manufacturers(brand_mfr, path_mfr)
          VALUES (
            '".$_POST["form_title"]."', '".$mfrpath."'         
            )",$link);
        $_SESSION['message'] = "<p id='form-success'>Производитель успешно добавлен!</p>";
        $id = mysql_insert_id();

              if (empty($_POST["upload_image"])) {
                include("actions/upload-image-manufact.php");
                unset($_POST["upload_image"]);
              }

              
              // var_dump($newBrand);
          
              $sourcePage = file_get_contents('../products/temp.php');
              $newPage = fopen('../products/'.strtolower($newBrand).'.'.php, a);
              $createdPage = fwrite($newPage, $sourcePage);
              fclose($newPage);

          }  
  }
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php");
    ?>

    <div id="block-content">
      <div id="block-parameters">
         <p id="title-page">Добавление производителя</p>
      </div>
      <?php 
        if (isset($_SESSION['message'])) {
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        }
       ?>
          <form enctype="multipart/form-data" method="post" >
            <ul id="edit-tovar">
              <li>
                <label>Название</label>
                <input type="text" name="form_title" />
              </li>
            </ul>
            <label class="stylelabel">Основная картинка</label>
            <div id="baseimg-upload">
              <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
              <input type="file" name="upload_image" />
              <p align="right"><input type="submit" id="submit_form" name="submit_add" value="Добавить" /></p>
            </div>            
            </div>            
          </form>      

    </div>

  </div>
</body>
</html>
<?php 
} else {
    header("Location: login.php");
  }
 ?>
