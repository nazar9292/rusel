$(document).ready(function() {
	$('.delete').click(function() {
		var rel = $(this).attr("rel");

		$.confirm( {
			'title'		: 'Подтверждение удаления',
			'message'	: 'После удаления восстановление будет невозможно! Продолжить?',
			'buttons'	: {
				'Да'	: {
					'class' : 'blue',
					'action': function() {
						location.href = rel;
					}
				},
				'Нет'	: {
					'class' : 'gray',
					'action': function() {}
				}
			}
		});
	});

	$('#select-links').click(function() {
	$("#list-links").slideToggle(200);
	});

	$('.h3click').click(function(){
	$(this).next().slideToggle(400);
	});

	$('.delete-cat').click(function(){
		var selectid = $("#cat_type option:selected").val();

		if (!selectid) {
			$("#cat_type").css("borderColor","#F5A4A4");
		}
		else {
		$.ajax({
		type: "POST",
		url: "./actions/delete-category.php",
		data: "id="+selectid,
		dataType: "html",
		cache: false,
		success: function(data) {

		if (data == "delete") {
			$("#cat_type option:selected").remove();
		}

		}

		});
		
		}
	});

});