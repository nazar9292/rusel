<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
  
  include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='news.php' >Администраторы</a>";

  $action = $_GET["action"];
  if (isset($action)) {
    $id = (int)$_GET["id"];
    switch ($action) {
      case 'delete':
        $delete = mysql_query("DELETE FROM reg_admin WHERE id = '$id'", $link);
        break;
    }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php"); 

      $all_count = mysql_query("SELECT * FROM reg_admin", $link);
      $all_count_result = mysql_num_rows($all_count);
    ?>

    <div id="block-content">
      <div id="block-parameters">
      <div id="block-info">
        <p id="count-style">Всего администраторов - <strong><?php echo $all_count_result; ?></strong></p>
        <p align="right" id="add-style-admin"><a href="add_admin.php">Добавить администратора</a></p>
      </div>

      <ul id="block-admin">
        <li>
          <table border="1" cellspacing="0" cellpadding="5" width="100%">
           <th>Логин</th>
           <th>Имя</th>
           <th>Изменить</th>
           <th>Удалить</th>
        <?php         
      
          $result = mysql_query("SELECT * FROM reg_admin ORDER BY login ASC", $link);
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);

              do {
                echo '            
                     <tr>                     
                      <td>'.$row["login"].'</td>
                      <td>'.$row["name"].'</td>
                      <td><a class="green" href="edit_admin.php?id='.$row["id"].'">Изменить</a></td>
                      <td><a rel="admins.php?id='.$row["id"].'&action=delete" class="delete">Удалить</a></td>
                    </tr>
                    ';                
              } while ($row = mysql_fetch_array($result));          
        echo '
          </table>
        </li>
      </ul> 
      ';
        }
   
?>
    <div id="footerfix"></div>
    </div>
    
  </div>
</body>
</html>
<?php }
  else {
    header("Location: login.php");
  }
 ?>
