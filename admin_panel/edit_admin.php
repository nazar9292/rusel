<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
  
  include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='admins.php' >Администраторы</a> \ <a> Изменение администраторов</a>";

  $id = clear_string($_GET["id"]);
  $action = clear_string($_GET["action"]);

  if ($_POST["submit_save"]) {
    $error = array();    

    if (!$_POST["form_name"]) {
      $error[] = "Укажите имя";
    }

    if (!$_POST["form_login"]) {
      $error[] = "Укажите логин";
    }

    if (!$_POST["form_password"]) {
      $error[] = "Укажите пароль";
    }

    if (count($error)) {
      $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
    }
    else {
        $querynew = "login='{$_POST["form_login"]}', pass='{$_POST["form_password"]}', name='{$_POST["form_name"]}'";
        $update = mysql_query("UPDATE reg_admin SET $querynew WHERE id = '$id'", $link);

        $_SESSION['message'] = "<p id='form-success'>Данные изменены!</p>";
              
          }  
  }
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php");
    ?>

    <div id="block-content">
      <div id="block-parameters">
         <p id="title-page">Изменение администраторов</p>
      </div>
      <?php 
        if (isset($_SESSION['message'])) {
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        }
       ?>

        <?php 
          $result = mysql_query("SELECT * FROM reg_admin WHERE id='$id'", $link);
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);
              do {
              echo '
                    <form method="post" >
                      <ul id="edit-tovar">
                        <li>
                          <label>Имя</label>
                          <input type="text" name="form_name" value="'.$row["name"].'" />
                        </li>
                       <li>
                          <label>Логин</label>
                          <input type="text" name="form_login" value="'.$row["login"].'" />
                      </li>
                      <li>
                          <label>Пароль</label>
                          <input type="password" name="form_password" value="'.$row["pass"].'" />
                      </li>
                    </ul>
                      <p align="right"><input type="submit" id="submit_form" name="submit_save" value="Сохранить" /></p>
                    </form>
                        ';

                     } while ($row = mysql_fetch_array($result));
            }
                   ?>                     

    </div>
  </div>
</body>
</html>
<?php 
} else {
    header("Location: login.php");
  }
 ?>
