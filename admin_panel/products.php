<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {

 
	include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='products.php' >Продукция</a>";

  $cat = $_GET["cat"];

  if (isset($cat)) {
    switch ($cat) {
      case 'all':
        $cat_name = 'Все товары';
        $url = "cat=all&";
        $cat = "";
        break;
      
        // case 'osram':
        // $cat_name = 'Osram';
        // $url = "cat=osram&";
        // $cat = "WHERE brand='Osram'";
        // break;

        // case 'siemens':
        // $cat_name = 'Siemens';
        // $url = "cat=siemens&";
        // $cat = "WHERE brand='Siemens'";
        // break;

        // case 'schneider_electric':
        // $cat_name = 'Schneider electric';
        // $url = "cat=schneider_electric&";
        // $cat = "WHERE brand='Schneider electric'";
        // break;

        // case 'paritet':
        // $cat_name = 'Паритет';
        // $url = "cat=paritet&";
        // $cat = "WHERE brand='Паритет'";
        // break;

        // case 'svet_tech':
        // $cat_name = 'Световые технологии';
        // $url = "cat=svet_tech&";
        // $cat = "WHERE brand='Световые технологии'";
        // break;

      default:
        $cat_name = $cat;
        $url = "cat=".clear_string($cat)."&";
        $cat = "WHERE brand='".clear_string($cat)."'";
        break;
    }    
  }
  else {
      $cat_name = 'Все товары';
      $url = "";
      $cat = "";
    }

  $action = $_GET["action"];
  if (isset($action)) {
    $id = (int)$_GET["id"];
    switch ($action) {
      case 'delete':
        $delete = mysql_query("DELETE FROM products WHERE id_prod = '$id'", $link);
        break;
    }
  }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="jquery_confirm/jquery_confirm.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php"); 

      $all_count = mysql_query("SELECT * FROM products", $link);
      $all_count_result = mysql_num_rows($all_count);
    ?>

    <div id="block-content">
      <div id="block-parameters">
        <ul id="options-list">
          <li>Продукции</li>
          <li><a href="#" id="select-links"><?php echo $cat_name; ?></a></li>
          <div id="list-links">
            <ul>
             <li><a href="products.php?cat=all"><strong>Все товары</strong></a></li>
             <?php  
              $result = mysql_query("SELECT * FROM manufacturers", $link);
              if (mysql_num_rows($result) > 0) {
                $row = mysql_fetch_array($result);
                do {
                  echo '           
                    <li><a href="products.php?cat='.$row["brand_mfr"].'"><strong>'.$row["brand_mfr"].'</strong></a></li>                    
                      ';                
                } while ($row = mysql_fetch_array($result)); 
                }    
              ?>
              
            </ul>
          </div>
        </ul>
      </div>
      <div id="block-info">
        <p id="count-style">Всего товаров - <strong><?php echo $all_count_result; ?></strong></p>
        <p align="right" id="add-style"><a href="add_product.php">Добавить продукт</a></p>
      </div>

      <ul id="block-tovar">
        <?php 

        $num = 6;

        $page = (int)$_GET['page'];

        $count = mysql_query("SELECT COUNT(*) FROM products $cat", $link); 
        $temp = mysql_fetch_array($count);
        $post = $temp[0];
        //общее число страниц
        $total = (($post - 1) / $num) + 1;
        $total = intval($total);
        //текущая страница
        $page = intval($page);
        if (empty($page) or $page < 0) $page = 1;
          if ($page > $total) $page = $total;

        $start = $page * $num - $num;

        if ($temp[0] > 0) {
          $result = mysql_query("SELECT * FROM products $cat ORDER BY id_prod DESC LIMIT $start, $num", $link);
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);

              do {
                echo '              
                  <li>
                  <p>'.$row["title_prod"].'</p>
                    <center>
                      <img src="../uploads_images/'.$row["image_prod"].'" width="120" height="120" alt="" class="fl" />
                    </center>
                    <p align="center" class="link-action">
                      <a class="green" href="edit_product.php?id='.$row["id_prod"].'">Изменить</a> | <a rel="products.php?id='.$row["id_prod"].'&action=delete" class="delete">Удалить</a>
                    </p>
                    </li>
                    ';                
              } while ($row = mysql_fetch_array($result));          
        echo '
      </ul> 
      ';
        }
      }

      if ($page != 1) $pervpage = '<li><a class"pstr-prev" href=products.php?'.$url.'page='.($page - 1).'"/>Назад</a></li>';
      if ($page != $total) $nextpage = '<li><a class="pstr-next" href="products.php?'.$url.'page='. ($page + 1) .'"/>Вперёд</a></li>';

// Находим две ближайшие станицы с обоих краев, если они есть
if($page - 5 > 0) $page5left = '<li><a href="products.php?'.$url.'page='. ($page - 5) .'">'. ($page - 5) .'</a></li>';
if($page - 4 > 0) $page4left = '<li><a href="products.php?'.$url.'page='. ($page - 4) .'">'. ($page - 4) .'</a></li>';
if($page - 3 > 0) $page3left = '<li><a href="products.php?'.$url.'page='. ($page - 3) .'">'. ($page - 3) .'</a></li>';
if($page - 2 > 0) $page2left = '<li><a href="products.php?'.$url.'page='. ($page - 2) .'">'. ($page - 2) .'</a></li>';
if($page - 1 > 0) $page1left = '<li><a href="products.php?'.$url.'page='. ($page - 1) .'">'. ($page - 1) .'</a></li>';

if($page + 5 <= $total) $page5right = '<li><a href="products.php?'.$url.'page='. ($page + 5) .'">'. ($page + 5) .'</a></li>';
if($page + 4 <= $total) $page4right = '<li><a href="products.php?'.$url.'page='. ($page + 4) .'">'. ($page + 4) .'</a></li>';
if($page + 3 <= $total) $page3right = '<li><a href="products.php?'.$url.'page='. ($page + 3) .'">'. ($page + 3) .'</a></li>';
if($page + 2 <= $total) $page2right = '<li><a href="products.php?'.$url.'page='. ($page + 2) .'">'. ($page + 2) .'</a></li>';
if($page + 1 <= $total) $page1right = '<li><a href="products.php?'.$url.'page='. ($page + 1) .'">'. ($page + 1) .'</a></li>';

if ($page+5 < $total)
{
    $strtotal = '<li><p class="nav-point">...</p></li><li><a href="products.php?'.$url.'page='.$total.'">'.$total.'</a></li>';
}else
{
    $strtotal = ""; 
}
   
?>
    <div id="footerfix"></div>
    <?php
  if ($total > 1)
{
    echo '
    <center>
    <div class="pstrnav">
    <ul>   
    ';
    echo $pervpage.$page5left.$page4left.$page3left.$page2left.$page1left."<li><a class='pstr-active' href='tovar.php?".$url."page=".$page."'>".$page."</a></li>".$page1right.$page2right.$page3right.$page4right.$page5right.$strtotal.$nextpage;
    echo '
    </center>   
    </ul>
    </div>
    ';
} 
?>
    </div>

  </div>
</body>
</html>
<?php }
  else {
    header("Location: login.php");
  }
 ?>