<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
  
	include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a>";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php"); 
    ?>

    <div id="block-content">
      <div id="block-parameters">
        <p id="title-page">Общая статистика</p>        
      </div>
      <div id="main-info">
        <?php 
          $result_product = mysql_query("SELECT * FROM products");
          $count_product = mysql_num_rows($result_product);

          $result_manufc = mysql_query("SELECT * FROM manufacturers");
          $count_manufc = mysql_num_rows($result_manufc);          

          $result_news = mysql_query("SELECT * FROM news");
          $count_news = mysql_num_rows($result_news);
         ?>

        <p class="all-count">Всего производителей: <strong><?php echo $count_manufc; ?></strong></p>
        <p class="all-count">Всего продукции: <strong><?php echo $count_product; ?></strong></p>
        <p class="all-count">Всего новостей: <strong><?php echo $count_news; ?></strong></p>
      </div>
      <div id="users_msg">
      <h2 id="users_msg_title">Сообщения от посетителей</h2>
      <ul>
        <li>
          <table bgcolor="#F1ECF7" border="1" cellspacing="0" cellpadding="4" width="100%">
           <th>Имя</th>
           <th>E-mail</th>
           <th>Сообщение</th>
           <th>Дата</th>
           <th>Ответить</th>
        <?php         
      
          $result = mysql_query("SELECT * FROM user_msg WHERE replied_msg = 0 ORDER BY date_msg DESC", $link);
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);

              do {
                echo '            
                     <tr>                     
                      <td>'.$row["name_msg"].'</td>
                      <td>'.$row["email_msg"].'</td>
                      <td width="50%">'.$row["text_msg"].'</td>
                      <td width="12%" style="font-size:13px;">'.$row["date_msg"].'</td>
                      <td width="1%"><a class="green" href="reply_msg.php?id='.$row["id_msg"].'">Ответить</a></td>
                    </tr>
                    ';                
              } while ($row = mysql_fetch_array($result));          
        echo '
          </table>
        </li>
      </ul> 
      ';
        }
   
        ?>
      </div>
    </div>
      
  </div>
</body>
</html>
<?php }
  else {
    header("Location: login.php");
  }
 ?>
