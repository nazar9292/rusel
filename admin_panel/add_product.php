<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
      
	include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='products.php' >Продукция</a> \ <a> Добавление товара</a>";

  if ($_POST["submit_add"]) {
    $error = array();

    if (!$_POST["form_title"]) {
      $error[] = "Укажите название товара";
    }

    if (!$_POST["form_category"]) {
      $error[] = "Укажите бренд";
    }
    else {
      $result = mysql_query("SELECT * FROM manufacturers WHERE brand_mfr='{$_POST["form_category"]}'",$link);
      $row = mysql_fetch_array($result);
      $selectbrand = translit($row["brand_mfr"]);
    }

    if (count($error)) {
      $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
    }
    else {
        $mfrpath = ''.$selectbrand.'.php';
        mysql_query("INSERT INTO products(title_prod,brand,mini_desc_prod,description_prod,path_prod)
          VALUES (
            '".$_POST["form_title"]."',
            '".$selectbrand."',
            '".$_POST["txt1"]."',
            '".$_POST["txt2"]."',
            '".$mfrpath."'
            )",$link);
        $_SESSION['message'] = "<p id='form-success'>Товар успешно добавлен!</p>";
        $id = mysql_insert_id();

              if (empty($_POST["upload_image"])) {
                include("actions/upload-image.php");
                unset($_POST["upload_image"]);
              }

              // $_POST['id_item'] = $id;
              // $_POST['brand'] = $selectbrand;
              // var_dump($selectbrand);
              
              $sourcePageItem = file_get_contents('../products/temp_item.php');
              $newPageItem = fopen('../products/'.strtolower($selectbrand).'_item.'.php, a);
              $createdPageItem = fwrite($newPageItem, $sourcePageItem);
              fclose($newPageItem);
          }  
  }
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php");
    ?>

    <div id="block-content">
      <div id="block-parameters">
         <p id="title-page">Добавление товара</p>
      </div>
      <?php 
        if (isset($_SESSION['message'])) {
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        }
       ?>
          <form enctype="multipart/form-data" method="post" >
            <ul id="edit-tovar">
              <li>
                <label>Название продукта</label>
                <input type="text" name="form_title" />
              </li>
              <li>
                <label>Бренд</label>
                <select name="form_category" size="1">
                  <?php 
                    $category = mysql_query("SELECT * FROM manufacturers", $link);

                    if (mysql_num_rows($category) > 0) {
                      $result_category = mysql_fetch_array($category);
                      do {
                        echo '
                            <option value"'.$result_category["brand_mfr"].'" >'.$result_category["brand_mfr"].'</option>
                        ';
                      } while ($result_category = mysql_fetch_array($category));
                    }
                   ?>
                </select>
              </li>
            </ul>
            <label class="stylelabel">Основная картинка</label>
            <div id="baseimg-upload">
              <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
              <input type="file" name="upload_image" />
            </div>
            <h3 class="h3click">Краткое описание продукта</h3>
            <div class="div-editor1">
              <textarea name="txt1" id="editor1" cols="100" rows="20"></textarea>
                <script type="text/javascript">
                  var ckeditor1 = CKEDITOR.replace("editor1");
                  AjaxFileManager.init({
                    returnTo: "ckeditor", 
                    editor: ckeditor1
                  });
                </script>
            </div>
            <h3 class="h3click"></p>Описание продукта</h3>
            <div class="div-editor2">
              <textarea name="txt2" id="editor2" cols="100" rows="20"></textarea>
                <script type="text/javascript">
                  var ckeditor2 = CKEDITOR.replace("editor2");
                  AjaxFileManager.init({
                    returnTo: "ckeditor", 
                    editor: ckeditor2
                  });
                </script>
            </div>
            <p align="right"><input type="submit" id="submit_form" name="submit_add" value="Добавить товар" /></p>
          </form>      

    </div>

  </div>
</body>
</html>
<?php 
} else {
    header("Location: login.php");
  }
 ?>
