<?php
session_start();
	include("./include/db_connect.php");
	// define('mysell', true);
	
	$admin_name = $_SESSION['admin_name'];
?>

<div id="block-header">

	<div id="block-header1">
		<h3>Панель управления</h3>
		<p id="link-nav"><?php echo $_SESSION['urlpage']; ?></p>
	</div>

	<div id="block-header2">
		<p align="right"><a href="admins.php">Администраторы</a>| <a href="?logout">Выход</a></p>
		<p align="right">Вы - <span><?php echo $admin_name; ?></span></p>
	</div>	

</div>

<div id="left-nav">
	<ul>
		<li><a href="products.php">Продукция</a></li>
		<li><a href="manufact.php">Производители</a></li>
		<li><a href="news.php">Новости</a></li>
		<li><a href="admins.php">Администраторы</a></li>
	</ul>
</div>