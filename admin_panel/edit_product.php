<?php
  session_start();

  if ( $_SESSION['auth_admin'] == "yes_auth") {
  
  // define('mysell', true);
  include("include/db_connect.php");
  include("include/functions.php");

  if (isset($_GET["logout"])) {
    unset($_SESSION['auth_admin']);
    header("Location: login.php");
  }

  $_SESSION['urlpage'] = "<a href='dashboard.php' >Главная</a> \ <a href='products.php' >Продукция</a> \ <a> Изменение товара</a>";

  $id = clear_string($_GET["id"]);
  $action = clear_string($_GET["action"]);
  if (isset($action)) {
    switch ($action) {
      case 'delete':
        if (file_exists("../uploads_images/".$_GET["img"])) {
          unlink("../uploads_images/".$_GET["img"]);
        }
        break;
    }
  }

  if ($_POST["submit_save"]) {
    $error = array();    

    if (!$_POST["form_title"]) {
      $error[] = "Укажите название товара";
    }

    if (!$_POST["form_category"]) {
      $error[] = "Укажите бренд";
    }
    else {
      $result = mysql_query("SELECT * FROM manufacturers WHERE brand_mfr='{$_POST["form_category"]}'",$link);
      $row = mysql_fetch_array($result);
      $selectbrand = $row["brand_mfr"];
      $brand_list = "";
    }

    if (empty($_POST["upload_image"])) {
        include("actions/upload-image.php");
        unset($_POST["upload_image"]);
      }

    if (count($error)) {
      $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
    }
    else {
        $querynew = "title_prod='{$_POST["form_title"]}', brand='$selectbrand', mini_desc_prod='{$_POST["txt1"]}',description_prod='{$_POST["txt2"]}'";
        $update = mysql_query("UPDATE products SET $querynew WHERE id_prod='$id'", $link);

        $_SESSION['message'] = "<p id='form-success'>Товар успешно изменен!</p>";
              
          }  
  }
 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Панель управления</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/edit.css" rel="stylesheet" type="text/css" />
<link href="jquery_confirm/jquery_confirm.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script_confirm.js"></script>
<script type="text/javascript" src="./ckeditor/ckeditor.js"></script>

</head>
<body>
  <div id="block-body">
    <?php 
      include("include/block_header.php");
    ?>

    <div id="block-content">
      <div id="block-parameters">
         <p id="title-page">Изменение продукции</p>
      </div>
      <?php 
        if (isset($_SESSION['message'])) {
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        }
       ?>

        <?php 
          $result = mysql_query("SELECT * FROM products WHERE id_prod='$id'", $link);
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);
              do {
              echo '
                    <form enctype="multipart/form-data" method="post" >
                      <ul id="edit-tovar">
                        <li>
                          <label>Название продукта</label>
                          <input type="text" name="form_title" value="'.$row["title_prod"].'" />
                        </li>
                   
                  ';          
              
                        $category = mysql_query("SELECT * FROM manufacturers", $link);

                        if (mysql_num_rows($category) > 0) {
                          $result_category = mysql_fetch_array($category);
                          
                          if ($row["brand"] == "Osram") $brand_list = "selected";
                          if ($row["brand"] == "Siemens") $brand_list = "selected";
                          if ($row["brand"] == "Паритет") $brand_list = "selected";
                          if ($row["brand"] == "Световые технологии") $brand_list = "selected";
                          if ($row["brand"] == "Schneider electric") $brand_list = "selected";

                          echo '
                          <li> 
                          <label>Бренд</label>
                            <select name="form_category" size="1">';
                             
                             do {
                                  echo '<option '.$brand_list.' value"='.$result_category["brand_mfr"].'" >'.$result_category["brand_mfr"].'</option>';
                           } while ($result_category = mysql_fetch_array($category));
                           
                           echo '
                                
                                </select>
                                </li>
                            ';
                         
                        }
                        echo '                              
                              </ul>
                              ';
                                if (strlen($row["image_prod"]) > 0 && file_exists("../uploads_images/".$row["image_prod"])) {
                                                                   
                                  echo '
                                  <label class="stylelabel">Основная картинка</label>
                                    <div id="baseimg">
                                      <img src="../uploads_images/'.$row["image_prod"].'" width="110" height="110" />
                                      <a href = "edit_product.php?id='.$row["id_prod"].'&img='.$row["image_prod"].'&action=delete" ></a>
                                    </div>
                              ';
                                }
                                else {
                                  echo '
                                  <label class="stylelabel">Основная картинка</label>
                                    <div id="baseimg-upload">
                                      <input type="hidden" name="MAX_FILE_SIZE" value="5000000" />
                                      <input type="file" name="upload_image" />
                                    </div>
                              ';
                                }

                              echo '
                              <h3 class="h3click">Краткое описание продукта</h3>
                              <div class="div-editor1">
                                <textarea name="txt1" id="editor1" cols="100" rows="20">'.$row["mini_desc_prod"].'</textarea>
                                  <script type="text/javascript">
                                    var ckeditor1 = CKEDITOR.replace("editor1");
                                    AjaxFileManager.init({
                                      returnTo: "ckeditor", 
                                      editor: ckeditor1
                                    });
                                  </script>
                              </div>
                              <h3 class="h3click"></p>Описание продукта</h3>
                              <div class="div-editor2">
                                <textarea name="txt2" id="editor2" cols="100" rows="20">'.$row["description_prod"].'</textarea>
                                  <script type="text/javascript">
                                    var ckeditor2 = CKEDITOR.replace("editor2");
                                    AjaxFileManager.init({
                                      returnTo: "ckeditor", 
                                      editor: ckeditor2
                                    });
                                  </script>
                              </div>
                              <p align="right"><input type="submit" id="submit_form" name="submit_save" value="Сохранить" /></p>
                    </form>
                        ';

                     } while ($row = mysql_fetch_array($result));
            }
                   ?>                     

    </div>
  </div>
</body>
</html>
<?php 
} else {
    header("Location: login.php");
  }
 ?>
