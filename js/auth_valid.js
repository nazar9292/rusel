$(document).ready(function() {
    $('#form_auth').validate (
    {
      rules: {
        "auth_login": {
          required: true,
          minlength: 5,
          maxlength: 15
          // remote: {
          //   type: "post",
          //   url: "/reg/check_login.php"
          // }
        },
        "auth_pasw": {
          required: true,
          minlength: 6,
          maxlength: 15
        }
      },
      messages: {
        "auth_login": {
          required: "Укажите Логин"
        },
        "auth_pasw": {
          required: "Укажите Пароль"
        }
        },

        submitHandler: function(form) {
          $(form).ajaxSubmit({
            success: function(data) {
              if (data == 'true') {
                $(".form_auth").fadeOut(300,function() {
                  $("#auth_message").addClass("reg_message_good").fadeIn(400).html("Вы успешно авторизованы");
                  $("#auth_form_submit").hide();
                });
              }
              else
              {
                $("#auth_message").addClass("#reg_message_error").fadeIn(400).html(data);
              }
            }
          });
        }
    });
  });
