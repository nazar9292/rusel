  $(document).ready(function() {
    $('#form_reg').validate (
    {
      rules: {
        "reg_login": {
          required: true,
          minlength: 5,
          maxlength: 15,
          remote: {
            type: "post",
            url: "/reg/check_login.php"
          }
        },
        "reg_pasw": {
          required: true,
          minlength: 6,
          maxlength: 15
        },
        "reg_name": {
          required: true,
          minlength: 2,
          maxlength: 15
        },
        "reg_email": {
          required: true,
          email: true
        },
        "reg_captcha": {
          required: true,
          remote: {
            type: "post",
            url: "/reg/check_captcha.php"
          }
        }
      },
      messages: {
        "reg_login": {
          required: "Укажите Логин",
          minlength: "От 5 до 15 символов",
          maxlength: "От 5 до 15 символов",
          remote: "Логин занят"
        },
        "reg_pasw": {
          required: "Укажите Пароль",
          minlength: "От 6 до 15 символов",
          maxlength: "От 6 до 15 символов",
        },
        "reg_name": {
          required: "Укажите Имя",
          minlength: "От 2 до 15 символов",
          maxlength: "От 2 до 15 символов",
        },
        "reg_email": {
          required: "Укажите E-mail",
          email: "Не корректный E-mail"
        },
        "reg_captcha": {
          required: "Введите код с картинки",
          remote: "Не верный код проверки"
          }
        },

        submitHandler: function(form) {
          $(form).ajaxSubmit({
            success: function(data) {
              if (data == 'true') {
                $(".form_reg").fadeOut(300,function() {
                  $("#reg_message").addClass("reg_message_good").fadeIn(400).html("Вы успешно зарегистрированы");
                  $("#reg_form_submit").hide();
                });
              }
              else
              {
                $("#reg_message").addClass("#reg_message_error").fadeIn(400).html(data);
              }
            }
          });
        }
    });
  });

// $(document).ready(function() {
//     $('#form_auth').validate (
//     {
//       rules: {
//         "auth_login": {
//           required: true,
//           minlength: 5,
//           maxlength: 15
//           // remote: {
//           //   type: "post",
//           //   url: "/reg/check_login.php"
//           // }
//         },
//         "auth_pasw": {
//           required: true,
//           minlength: 6,
//           maxlength: 15
//         }
//       },
//       messages: {
//         "auth_login": {
//           required: "Укажите Логин"
//         },
//         "auth_pasw": {
//           required: "Укажите Пароль"
//         }
//         },

//         submitHandler: function(form) {
//           $(form).ajaxSubmit({
//             success: function(data) {
//               if (data == 'true') {
//                 $(".form_auth").fadeOut(300,function() {
//                   $("#auth_message").addClass("reg_message_good").fadeIn(400).html("Вы успешно авторизованы");
//                   $("#auth_form_submit").hide();
//                 });
//               }
//               else
//               {
//                 $("#auth_message").addClass("#reg_message_error").fadeIn(400).html(data);
//               }
//             }
//           });
//         }
//     });
//   });
