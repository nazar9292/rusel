<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'clients';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Работа с </span> клиентами</h2>
          <div class="clr"></div>
          <p>Работая с компанией Паритет, вы уверены, что на складе нашего предприятия имеется в наличии постоянный запас всей номенклатуры по прайс-листу. Продукцию мы отгружаем в минимальные сроки - вплоть до 1 дня.</p>
          <p>В каком бы регионе России или СНГ ни находилось ваше предприятие - мы можем полностью взять на себя организацию доставки вашего заказа.</p>
          <p> Если же вам удобнее самим забрать заказ (самовывоз) – мы ждем вас по будням с 08:00 до 17:00. Для быстрого обслуживания при самовывозе, пожалуйста, сообщайте нам заранее планируемое время вашего визита!</p>          
        </div>
        <div class="article">
          <h2 id="contract_client"><span>Типовые договора поставок</span></h2>
          <div class="clr"></div>
          <p>Мы поставляем нашу продукцию как Российским так и зарубежным предприятиям. Согласно законодательству Российской Федерации поставки мы ведем на основе заключенных договоров с покупателями. На основе законодательства и с учетом накопленной за годы работы информации о потребностях наших клиентов нами составлены типовые договора поставок. В данном разделе вы можете ознакомиться с данными типовыми договорами.</p>
        </div>
      </div>
       <?php
            require_once "blocks/sidebar.php";
        ?>
      <div class="clr"></div>
    </div>
  </div>
    <?php
        require_once "blocks/footer.php";
    ?>
</div>
</body>
</html>
