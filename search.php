<?php
session_start();
	include("db_connect.php");  
  include("admin_panel/include/functions.php");

  $search = clear_string($_GET["editbox_search"]);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL - <?php echo $search; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  	<?php
		require_once "blocks/header.php";
	?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
        <?php 
        		
            $count = mysql_query("SELECT * FROM products WHERE title_prod LIKE '%$search%' OR brand LIKE '%$search%'", $link);
            $count_search = mysql_num_rows($count);            
            if (strlen($search) >= 3 && strlen($search) < 32) {              
            echo '<h3 id="count_search">Найдено: '.$count_search.'</h3>';
            $result = mysql_query("SELECT * FROM products WHERE title_prod LIKE '%$search%' OR brand LIKE '%$search%'", $link);
           
        		if (mysql_num_rows($result) > 0) {
        			$row = mysql_fetch_array($result);

        			do {
        				echo '                
							<h2>'.$row["title_prod"].'</h2>
				          <div class="clr"></div>
				          <div class="img"><img src="uploads_images/'.$row["image_prod"].'" width="100" height="110" alt="" class="fl" /></div>
				          <div class="post_content">
				            <p>'.$row["mini_desc_prod"].'</p>
				            <p class="spec"><a href="products/'.translit($row["path_prod"]).'?brand='.translit($row["brand"]).'" class="rm">Подробнее</a></p>
				          </div>
        				';
        			} while ($row = mysql_fetch_array($result));
        		}
            }
            else
            {
              echo '<h3 id="count_search">Значение поиска должно быть от 3 до 32 символов!</h3>';
            }
        ?>          
          <div class="clr"></div>
        </div>
      </div>
      <?php
		require_once "blocks/sidebar.php";
		?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
		require_once "blocks/footer.php";
	?>
</div>
</body>
</html>
