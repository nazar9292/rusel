<?php
session_start();
	include("db_connect.php");  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  	<?php
    $page = 'index';
		require_once "blocks/header.php";
	?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
        <?php 
        		$result = mysql_query("SELECT * FROM news ORDER BY `date` DESC", $link);

        		if (mysql_num_rows($result) > 0) {
        			$row = mysql_fetch_array($result);

        			do {
        				echo '
							<h2>'.$row["title"].'</h2>
          					<p class="infopost">Опубликовано <span class="date">'.$row["date"].'</span></p>
				          <div class="clr"></div>
				          <div class="img"><img src="uploads_images/'.$row["image"].'" width="200" height="210" alt="" class="fl" /></div>
				          <div class="post_content">
				            <p>'.$row["text"].'</p>
				          </div>
        				';
        			} while ($row = mysql_fetch_array($result));
        		}

        ?>          
          <div class="clr"></div>
        </div>
      </div>
      <?php
		require_once "blocks/sidebar.php";
		?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
		require_once "blocks/footer.php";
	?>
</div>
</body>
</html>
