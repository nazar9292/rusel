<?php
	include("db_connect.php");
  session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" src="js/jquery.validate.js"></script>
<script type="text/javascript" src="js/auth_reg_valid.js"></script>
<script type="text/javascript" src="js/auth_valid.js"></script>

</head>
<body>
<div class="main">
  	<?php
		require_once "products/blocks/header.php";
	?>
  <div class="content">
    <div class="content_resize">   
         <p id="auth_message"></p>       
        <div class="form_auth">
          <h2 align="center">АВТОРИЗАЦИЯ</h2>         
           <form method="post" id="form_auth" action="/reg/handler_auth.php">
            <ul id="form_auth_ul">
              <li>
              <label>Логин</label>
              <span class="star">*</span>
              <input type="text" name="auth_login" id="auth_login" />
              </li>
              <li>
              <label>Пароль</label>
              <span class="star">*</span>
              <input type="password" name="auth_pasw" id="auth_pasw" />
              </li>
            </ul>      
            <input type="submit" name="auth_submit" id="auth_form_submit" value="Войти" />
            </form>
            <p>Если вы не зарегистрированы, <span id="reg_note">зарегистрируйтесь.</span></p>
        </div>
          
          <p id="reg_message"></p>
    <div class="form_reg">
          <h2 align="center">РЕГИСТРАЦИЯ</h2>           
           <form method="post" id="form_reg" action="/reg/handler_reg.php">
            <ul id="form_reg_ul">
              <li>
              <label>Логин</label>
              <span class="star">*</span>
              <input type="text" name="reg_login" id="reg_login" />
              </li>
              <li>
              <label>Пароль</label>
              <span class="star">*</span>
              <input type="password" name="reg_pasw" id="reg_pasw" />
              </li>
              <li>
              <label>Имя</label>
              <span class="star">*</span>
              <input type="text" name="reg_name" id="reg_name" />
              </li>
              <li>
              <label>E-mail</label>
              <span class="star">*</span>
              <input type="text" name="reg_email" id="reg_email" />
              </li>
              <li>
                <div id="block_captcha">
                  <img src="reg/reg_captcha.php" />
                  <input type="text" name="reg_captcha" id="reg_captcha" />
                  <p id="reloadcaptcha">Обновить</p>
                </div>
              </li>
            </ul>          
           <input type="submit" name="reg_submit" id="reg_form_submit" value="Регистрация" />
          </form> 
        </div>
      
      <div class="clr"></div>
    </div>
  </div>
  <?php
		require_once "blocks/footer.php";
	?>
</div> 
<script type="text/javascript" src="js/scripts.js"></script>
</body>
</html>
