<?php
  session_start();

  include("db_connect.php");
  include("functions/functions.php");

  if ($_POST["submit_msg"]) {
    $error = array();

    if (!$_POST["name_msg"]) {
      $error[] = "Укажите имя";
    }

    if (!$_POST["email_msg"]) {
      $error[] = "Укажите e-mail";
    }

    if (!$_POST["message_msg"]) {
      $error[] = "Напишите ваше сообщение";
    }

    if (count($error)) {
      $_SESSION['message'] = "<p id='form-error'>".implode('<br />', $error)."</p>";
    }
    else {
        mysql_query("INSERT INTO user_msg(name_msg, email_msg, text_msg, replied_msg, date_msg)
          VALUES (
            '".$_POST["name_msg"]."',
            '".$_POST["email_msg"]."',
            '".$_POST["message_msg"]."',
            '0',
            '".date("Y-m-d")."'
            )",$link);
        $_SESSION['message'] = "<p id='form-success'>Сообщение отправлено!</p>";
        $id = mysql_insert_id();

          }  

  }
  else {
   unset($_POST["name_msg"]);
   unset($_POST["email_msg"]);
   unset($_POST["message_msg"]);
  }   
 
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'support';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">      
       <div class="article">
          <h2>Техническая поддержка</h2>
          <div class="clr"></div>
          <p>Центр поддержки (Support Center) Rusel — это быстрое и качественное обслуживание партнеров и клиентов компании по всей России, оперативное решение вопросов и быстрое предоставление запрошенной клиентом информации.</p>
        </div>  
        <div class="article">
        <label id="result_msg"></label>
        <?php 
        if (isset($_SESSION['message'])) {
          echo $_SESSION['message'];
          unset($_SESSION['message']);
        }
       ?>          
        </div>
        <p><strong>Тел./Факс: (843) 271-25-96</strong></p>
        <p><strong>E-mail: info@rusel.ru</strong></p>
      </div>
      <?php
          require_once "blocks/sidebar.php";
      ?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
    require_once "blocks/footer.php";
  ?>
</div>
</body>
</html>
