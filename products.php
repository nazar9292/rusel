<?php 
session_start();
  include("db_connect.php");
  include("admin_panel/include/functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'products';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="manufact_view">
          <h2>Производители</h2>
          <div class="clr"></div>
          <?php 
            $result = mysql_query("SELECT * FROM manufacturers", $link);

            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);              
              do {
                echo '              
                  
                  <div class="img_manf"><a href="'.$row["path_mfr"].'?brand='.translit($row["brand_mfr"]).'"><img src="uploads_images/'.$row["image"].'" width="200" height="200" alt="" class="fl" /></a>
                  <h2><a href="'.$row["path_mfr"].'?brand='.translit($row["brand_mfr"]).'">'.$row["brand_mfr"].'</a></h2></div>
                ';
              } while ($row = mysql_fetch_array($result));

            }

        ?>          
        </div>
      </div>
      <?php
          require_once "blocks/sidebar.php";
      ?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
    require_once "blocks/footer.php";
  ?>
</div>
</body>
</html>
