<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'about';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2>О Компании</h2>
          <div class="clr"></div>
          <p><strong>Компания Rusel является дистрибьютером компании в области электрооборудования и поставщиком комплексных энергоэффективных решений для энергетики и инфраструктуры, промышленных предприятий, объектов гражданского и жилищного строительства, а также центров обработки данных.</strong></p>
          <p>Помимо внедрения энергоэффективных решений компания предлагает российским клиентам сервисное обслуживание, эффективную логистику, энергоаудит, передачу инновационных технологий и обучение.</p>
        </div>
        <div class="article">
          <h2>Наши цели</h2>
          <div class="clr"></div>
          <p>Rusel считает своей целью поддержание высокой прибыльности и стабильности своего бизнеса. Для достижения этих целей Rusel будет использовать все доступные возможности, включая рост эффективности своих операций, улучшение качества предоставляемых услуг, применению новых прогрессивных технологий.</p>
        </div>
      </div>
      <?php
           require_once "blocks/sidebar.php";
      ?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
    require_once "blocks/footer.php";
  ?>
</div>
</body>
</html>
