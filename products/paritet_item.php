<?php 
  include("../db_connect.php");
  session_start();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="image/x-icon" rel="shortcut icon" href="../images/RuselIcon.jpg">
<link type="text/css" rel="stylesheet" href="../css/style.css"   />
<link type="text/css" rel="stylesheet" href="../css/coin-slider.css" />
<link type="text/css" href="../css/edit.css" rel="stylesheet"  />
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
<script type="text/javascript" src="../js/coin-slider.min.js"></script>
</head>
<body>

<div class="main">
  	﻿  <?php
          require_once "blocks/header.php";
      ?>
  </div>  
  <div class="content">  
    <div class="content_resize">
      <div class="mainbar">
      <h2 id="main_text">Продукция от Паритет</h2>
        <div class="article">
        <?php 
         $id = $_GET['id'];

            $result = mysql_query("SELECT * FROM products WHERE id_prod = $id AND brand = 'Паритет';", $link);
       
            if (mysql_num_rows($result) > 0) {
              $row = mysql_fetch_array($result);
              
                echo '
              <h2><a href="'.$row["path_prod"].'">'.$row["title_prod"].'</a></h2>
                  <div class="clr"></div>
                  <div class="img"><img src="../uploads_images/'.$row["image_prod"].'" width="150" height="160" alt="" class="fl" /></div>
                  <div class="post_content">
                   <pre><p>'.$row["description_prod"].'</p></pre>
                  </div>
                   
                ';
            }
        ?>          
          <div class="clr"></div>
        </div>
      </div>
      ﻿     <?php
                require_once "blocks/sidebar.php";
            ?>  
      <div class="clr"></div>
    </div>
  </div>
  ﻿   <?php
        require_once "blocks/footer.php";
      ?>
  </div>
</body>
</html>
