﻿  <div class="header">
    <div class="header_resize">
      <div class="searchform">

        <form id="formsearch" name="formsearch" method="GET" action="../search.php">
          <span>
          <input name="editbox_search" class="editbox_search" id="editbox_search" maxlength="80" placeholder="Поиск на сайте:" type="text" />
          </span>
          <input name="button_search" src="../images/search.gif" class="button_search" type="image" />
        </form>
        
      </div>
      <div class="menu_nav">
        <ul>
        <a href="https://rusel.ru"><img src="../images/RuselLogo.png" alt="ruselLogo" /></a>   
          <li><a href="../index.php"><span>Главная</span></a></li>
          <li>
              <div class="dropdown">
                <a href="../clients.php"><span>Покупателям</span></a>
                <div class="dropdown_content">
                  <a href="../clients.php">Работа с клиентами</a>
                  <a href="../clients.php #contract_client">Договоры</a>
                </div>
          </div>
          </li>
          <li class="active"><a href="../products.php"><span>Продукция</span></a></li>
          <li><a href="../support.php"><span>Поддержка</span></a></li>
          <li><a href="../about.php"><span>О нас</span></a></li>          
          <li><a href="../contact.php"><span>Контакты</span></a></li>
        </ul>
      </div>
      <div class="clr"></div>
      <div class="clr"></div>
    </div>
  </div>