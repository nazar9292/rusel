﻿      <div class="sidebar">
        <div class="gadget">
          <div class="clr"></div>
          <ul class="sb_menu">
            <li><a href="../index.php">Главная</a></li>
            <li><a href="../clients.php">Покупателям</a></li>
            <li><a href="../products.php">Продукция</a></li>            
            <li><a href="../support.php">Поддержка</a></li>
            <li><a href="../about.php">О нас</a></li>
            <li><a href="../contact.php">Контакты</a></li>
          </ul>
        </div>
        <div class="gadget">
          <h2 class="star"><span>Производители</span></h2>
          <div class="clr"></div>
          <ul class="ex_menu">
            <li><a href="http://schneider-electric.com" target="_blank">Schneider electric</a><br /></li>
            <li><a href="http://www.siemens.com" target="_blank">Siemens</a><br /></li>
            <li><a href="http://www.paritet-podolsk.ru" target="_blank">Паритет</a><br /></li>
            <li><a href="http://ltcompany.com" target="_blank">Световые технологии</a><br /></li>
            <li><a href="http://www.osram.ru" target="_blank">Osram</a><br /></li>
          </ul>
        </div>
      </div>