﻿  <div class="fbg">
    <div class="fbg_resize">
      <div class="col c2">
        <h2><span>Центр поддержки</span> клиентов</h2>
        <p>Мы обеспечим Вас: </p>
        <ul class="fbg_ul">
          <li>качественной диагностикой</li>
          <li>эффективными инструментами</li>
          <li>оперативную помощь</li>
        </ul>
      </div>
      <div class="col c3" style="float: right;">
        <h2><span>Свяжитесь с </span> нами</h2>
        <p>По всем вопросам, пожалуйста, обращайтесь к нам по</p>
        <p class="contact_info"> <span>Адрес:</span>Казань ул. Халилова 18, Респ. Татарстан<br />
          <span>Телефон:</span> (843) 271-25-96<br />
          <span>FAX:</span> (843) 271-25-97<br />
          <span>E-mail:</span> info@rusel.ru </p>
      </div>
      <div class="clr"></div>
    </div>
  </div>