<?php
  session_start();
	include("../db_connect.php");
  $selectedBrand = $_GET['brand'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="image/x-icon" rel="shortcut icon" href="../images/RuselIcon.jpg">
<link type="text/css" rel="stylesheet" href="../css/style.css"   />
<link type="text/css" rel="stylesheet" href="../css/coin-slider.css" />
<link type="text/css" href="../css/edit.css" rel="stylesheet"  />
<script type="text/javascript" src="../js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../js/script.js"></script>
<script type="text/javascript" src="../js/coin-slider.min.js"></script>
</head>
<body>

<div class="main">
  	<?php
		require_once "../products/blocks/header.php";
	?>
  <div class="content">  
    <div class="content_resize">
      <div class="mainbar">
      <h2 id="main_text">Продукция от <?php echo $selectedBrand ?> </h2>
        <div class="article">
        <?php 
        		$result = mysql_query("SELECT * FROM products WHERE brand='".$selectedBrand."'", $link);

        		if (mysql_num_rows($result) > 0) {
        			$row = mysql_fetch_array($result);

        			do {
        				echo '
							<h2><a href="'.$row["brand"].'_item.php?id='.$row["id_prod"].'">'.$row["title_prod"].'</a></h2>
				          <div class="clr"></div>
				          <div class="img"><a href="'.$row["brand"].'_item.php?id='.$row["id_prod"].'"><img src="../uploads_images/'.$row["image_prod"].'" width="150" height="160" alt="" class="fl" /></a></div>
				          <div class="post_content">
				            <p>'.$row["mini_desc_prod"].'</p>
				            <p class="spec"><a href="'.$row["brand"].'_item.php?id='.$row["id_prod"].'" class="rm">Подробнее</a></p>
				          </div>
        				';
        			} while ($row = mysql_fetch_array($result));
        		}

        ?>          
          <div class="clr"></div>
        </div>
      </div>
      <?php
		require_once "../products/blocks/sidebar.php";
		?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
		require_once "../products/blocks/footer.php";
	?>
</div>
</body>
</html>
