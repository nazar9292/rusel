  <div class="header">
    <div class="header_resize">
      <div class="searchform">
        
        <form id="formsearch" name="formsearch" method="GET" action="search.php">
          <span>
          <input name="editbox_search" class="editbox_search" id="editbox_search" maxlength="80" placeholder="Поиск на сайте:" type="text" />
          </span>
          <input name="button_search" src="images/search.gif" class="button_search" type="image" />
        </form>

      </div>
      <div class="menu_nav">
        <ul>
        <a href="https://rusel.ru"><img src="images/RuselLogo.png" alt="ruselLogo" /></a>  
        
          <li <?php echo ($page == 'index') ? "class = 'active'": ""; ?>><a href="index.php"><span>Главная</span></a></li>
          <li <?php echo ($page == 'clients') ? "class = 'active'": ""; ?>>
              <div class="dropdown">
                <a href="clients.php"><span>Покупателям</span></a>
                <div class="dropdown_content">
                  <a href="clients.php" class="drop_link">Работа с клиентами</a>
                  <a href="clients.php #contract_client" class="drop_link">Договоры</a>
                </div>
          </div>
          </li>
          <li <?php echo ($page == 'products') ? "class = 'active'": ""; ?>><a href="products.php"><span>Продукция</span></a></li>
          <li <?php echo ($page == 'support') ? "class = 'active'": ""; ?>><a href="support.php"><span>Поддержка</span></a></li>
          <li <?php echo ($page == 'about') ? "class = 'active'": ""; ?>><a href="about.php"><span>О нас</span></a></li>          
          <li <?php echo ($page == 'contact') ? "class = 'active'": ""; ?>><a href="contact.php"><span>Контакты</span></a></li>
        </ul>
      </div>
      <div class="clr"></div>
      <div class="logo">
        <h1><a href="index.php"><span>RUSEL</span> <small>качество доступно </small></a></h1>              
      </div>
      <div class="clr"></div>
      <div class="slider">
        <div id="coin-slider"> <a href="#"><img src="images/slide1sized.jpg" width="960" height="360" alt="" /><span><big>Оборудования высокого качества</big><br />
          Компания Rusel специализируется продажей высококачественного оборудования</span></a> <a href="#"><img src="images/slide2sized.jpg" width="960" height="360" alt="" /><span><big>Оптимальные технические решения</big><br />
          </span></a> <a href="#"><img src="images/slide3sized.jpg" width="960" height="360" alt="" /><span><big>Гарантие на оборудование</big><br />
          </span></a> </div>
        <div class="clr"></div>
      </div>
      <div class="clr"></div>
    </div>
  </div>