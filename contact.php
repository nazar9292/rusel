<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'contact';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2><span>Контакты</span></h2>
          <div class="clr"></div>
          <p>Обратиться к нам или отправить факс вы можете по любому из нижеуказанных номеров: </p>
          <p><strong>Тел./Факс: (843) 271-25-96</strong></p>
          <p><strong>E-mail: info@rusel.ru</strong></p>

          <h2>Реквизиты</h2>
          <p>При оформлении с нами договоров и иных документов, требующих указания юридически корректной информации просим использовать нижеуказанные данные:</p>
          <p>Название: ООО "ТПД Паритет" </p>  
          <p>Юридический адрес: 420000, Респ. Татарстан, г. Казань, ул. Халилова 18 </p>
          <p>ИНН/КПП: 1658092044/165801001 </p>
          <p>ОКПО: 82325054 </p>
          <p>ОКОНХ: 14172, 71100, 71200 </p>
          <p>Наименование банка: Банк ВТБ (ПАО) г. Казань </p>
          <p>БИК: 044525187 </p>
          <p>Расчетный счет: 40702810310800000015  </p>
          <p>Корреспондентский счет: 30101810700000000187</p>
        </div>        
      </div>
      <?php
          require_once "blocks/sidebar.php";
      ?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
    require_once "blocks/footer.php";
  ?>
</div>
</body>
</html>
