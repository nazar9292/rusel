<?php
  session_start();

  include("db_connect.php");
  include("functions/functions.php"); 

  if ($_POST["submit_msg"]) {
    header("Location: user_msg.php");
  }
  else {
   unset($_POST["name_msg"]);
   unset($_POST["email_msg"]);
   unset($_POST["message_msg"]);
  }   
 
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>RuseL</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" type="image/x-icon" href="images/RuselIcon.jpg">
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/coin-slider.css" />
<link href="css/edit.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
  <?php
  $page = 'support';
    require_once "blocks/header.php";
  ?>
  <div class="content">
    <div class="content_resize">
      <div class="mainbar">
        <div class="article">
          <h2>Техническая поддержка</h2>
          <div class="clr"></div>
          <p>Центр поддержки (Support Center) Rusel — это быстрое и качественное обслуживание партнеров и клиентов компании по всей России, оперативное решение вопросов и быстрое предоставление запрошенной клиентом информации.</p>
        </div>
        <div class="article">
          <p>Клиенту не нужно тратить свое время на поиск сотрудника компании, который мог бы ответить на его вопрос.</p>
          <p>Центр поддержки клиентов — решение задач любого уровня сложности.</p>
          <p>Основные категории запросов, которые обрабатывает Центр поддержки клиентов:</p>
          <ul>
            <li>Вопросы о продукции (технического и нетехнического характера), прайс-листы;</li>
            <li>Вопросы о наличии продукции на складе, сроках реализации заказов и т.д.;</li>
            <li>Вопросы, связанные со статусом отгрузок продукции со склада и сроками поставки;</li>
            <li>Запросы на документацию (печатные и электронные каталоги, инструкции по установке и эксплуатации и т.д.).</li>
          </ul>
        </div>
        <div class="article">
          <h2><span>Напишите</span> нам</h2>
          <div class="clr"></div>
          <form action="user_msg.php#result_msg" method="post" id="sendemail">
            <ol>
              <li>
                <label>Имя</label>
                <input id="name_msg" name="name_msg" class="text" />
              </li>
              <li>
                <label>Email адрес</label>
                <input id="email_msg" name="email_msg" class="text" />
              </li>
              <li>
                <label>Ваше сообщение</label>
                <textarea id="message_msg" name="message_msg" rows="8" cols="50"></textarea>
              </li>
              <li>
                <input type="submit" name="submit_msg" id="submit_msg" value="Отправить" class="send" />
                <div class="clr"></div>
              </li>
            </ol>
          </form>
        </div>
        <p><strong>Тел./Факс: (843) 271-25-96</strong></p>
        <p><strong>E-mail: info@rusel.ru</strong></p>
      </div>
      <?php
          require_once "blocks/sidebar.php";
      ?>
      <div class="clr"></div>
    </div>
  </div>
  <?php
    require_once "blocks/footer.php";
  ?>
</div>
</body>
</html>
